import { Adresse } from "./Adresse";
import { Telephone } from "./Telephone";
import { Contact } from "./Contact";

export interface Entreprise {
    idEntreprise : number,
    IdTypeClient: number,
    raisonSociale: string,
    email: string,
    reference : string,
    siteWeb : string,
    numeroSiren : string,
    numeroSiret : string,
    codeNAF : string,
    capitalSocial : number,
    rcs : string,
    tvaIntra : string,
    isActif : boolean,
    IdCategorieTarifaire : number,
    adresses : Adresse[],
    telephones : Telephone[]
}







