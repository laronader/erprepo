import { Adresse } from "./Adresse";
import { Telephone } from "./Telephone";

export interface Contact {
    IdContact : number,
    prenom : string,
    nom : string,
    email: string,
    fonction : string,
    isActif : boolean,
    adresses : Adresse[],
    telephones : Telephone[]
}