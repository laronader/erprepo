export interface Adresse {
    idAdresse : number,
    isPrincipale : boolean,
    titre : string,
    complement1 : string,
    complement2 : string,
    complement3 : string,
    codePostal : string,
    ville : string,
    IdPays: number
    longitude : string,
    latitude : string
}