
import { createServerRenderer } from 'aspnet-prerendering';
import { Component, OnInit } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { Entreprise } from '../../models/Entreprise';
import { EntrepriseService } from '../../services/entreprise.service';

@Component({
  selector: 'app-liste-clients',
  templateUrl: './liste-clients.component.html',
  styleUrls: ['./liste-clients.component.css']
})
export class ListeClientsComponent implements OnInit {

  entreprises : Entreprise[];
  
 
  constructor(private entrepriseService : EntrepriseService) { }

  ngOnInit() {

    this.entrepriseService.getEntreprises()
      .subscribe(result => this.entreprises = result);
  }


}
