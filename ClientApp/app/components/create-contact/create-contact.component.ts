import { RouterModule, Router } from '@angular/router';
import { ContactService } from './../../services/contact.service';
import { ReferentielService } from './../../services/referentiel.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Contact } from '../../models/Contact';
import { ToastyService } from 'ng2-toasty';



@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.css']
})
export class CreateContactComponent implements OnInit{

  civilites;
  telephoneTypes;
  showRemoveBtn: boolean;
  showRemoveAdresseBtn: boolean;
  contactForm: FormGroup;
  contact: Contact;
  errorMessage: any;

 
  get telephones(): FormArray{
    return <FormArray>this.contactForm.get('telephones');
  }

  get adresses(): FormArray{
    return <FormArray>this.contactForm.get('adresses');
  }

  constructor(
    private referentielService: ReferentielService,
    private contactService : ContactService,
    private fb : FormBuilder,
    private router: Router,
    private toastyService : ToastyService
  ){}

  ngOnInit(){
    this.showRemoveBtn = false;
    this.referentielService.getCivilites()
      .subscribe(result => this.civilites = result);
    
    this.referentielService.getPhoneTypes()
      .subscribe(result => this.telephoneTypes = result);
    
    this.contactForm = this.fb.group({
      IdCivilite: [1, [Validators.required]],
      prenom: ['', [Validators.required]],
      nom: ['', [Validators.required]],
      fonction: ['', [Validators.required]],
      email: ['',[Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      telephones : this.fb.array([ this.buildTelephones() ]),
      adresses: this.fb.array([ this.buildAdresses() ]),
      facebookUrl: [''],
      twitterUrl: [''],
      linkedinUrl: [''],
      gplusUrl: [''],
      siteWeb: ['']
    });


    if(this.telephones.length > 1)
      this.showRemoveBtn = true;
    
    if(this.adresses.length > 1)
      this.showRemoveAdresseBtn = true;
    
  }

  addContact() {
    if(this.contactForm.dirty && this.contactForm.valid) {

      let c = Object.assign({}, this.contact, this.contactForm.value);
      console.log(c);
     
    }else if(!this.contactForm.dirty){
      this.onSaveComplete();
    }
  }

  onSaveComplete(){
    this.toastyService.success({
      title: 'Succès',
      msg: 'Le contact a été bien enregistré',
      theme: 'bootstrap',
      showClose: true,
      timeout: 5000
    });
    this.contactForm.reset();
    //this.router.navigate(['/']);
  }

  addTelephone(){
    this.showRemoveBtn = true;
    this.telephones.push(this.buildTelephones());
  }

  addAdresses(){
    this.showRemoveAdresseBtn = true;
    this.adresses.push(this.buildAdresses());
  }

  removeTelephone(i){
    if(this.telephones.length > 1){
      this.telephones.removeAt(i);
    }
    if(this.telephones.length == 1){
      this.showRemoveBtn = false;
    }
  }

  removeAdresse(i){
    if(this.adresses.length > 1)
      this.adresses.removeAt(i);
    
    if(this.adresses.length == 1)
      this.showRemoveAdresseBtn = false;
  }

  buildTelephones() : FormGroup {
    return this.fb.group({
      IdTypeTelephone : [1, [Validators.required]],
      numero: ['', [Validators.required]]
    })
  }

  buildAdresses() : FormGroup {
    return this.fb.group({
      titre: [''],
      complement1: ['', [Validators.required]],
      complement2: [''],
      complement3: [''],
      codePostal: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      IdPays: [1, [Validators.required]],
      isAdressePrincipale: false,
      longitude: [''],
      latitude: ['']
    })
  }


}
