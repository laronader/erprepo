import { ToastyService } from 'ng2-toasty';
import { Router } from '@angular/router';
import { EntrepriseService } from './../../services/entreprise.service';
import { ReferentielService } from './../../services/referentiel.service';
import { FormGroup, ReactiveFormsModule, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Entreprise } from './../../models/Entreprise';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-entreprise',
  templateUrl: './create-entreprise.component.html',
  styleUrls: ['./create-entreprise.component.css']
})
export class CreateEntrepriseComponent implements OnInit {

  typeClients;
  telephoneTypes;
  categorieTarifaire;

  showRemoveTelBtn: boolean;
  showRemoveAdresseBtn: boolean;

  entrepriseForm: FormGroup;
  entreprise: Entreprise;

  get telephones(): FormArray{
    return <FormArray>this.entrepriseForm.get('telephones');
  }

  get adresses(): FormArray{
    return <FormArray>this.entrepriseForm.get('adresses');
  }

  constructor(
    private referentielService: ReferentielService,
    private entrepriseService: EntrepriseService,
    private fb: FormBuilder,
    private router: Router,
    private toastyService: ToastyService
  ) { }

  ngOnInit() {
    this.showRemoveTelBtn = false;
    this.showRemoveAdresseBtn = false;

    this.referentielService.getPhoneTypes()
      .subscribe(result => this.telephoneTypes = result);
    
    this.referentielService.getCategorieTarifaire()
      .subscribe(result => this.categorieTarifaire = result);
    
    this.referentielService.getTypeClients()
      .subscribe(result => this.typeClients = result);
    
    this.entrepriseForm = this.fb.group({
      IdTypeClient: [1, [Validators.required]],
      raisonSociale: ['', [Validators.required]],
      reference: ['', [Validators.required]],
      siteWeb: [''],
      email: ['', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      IdCategorieTarifaire: [1, [Validators.required]],
      telephones: this.fb.array([this.buildTelephones()]),
      adresses: this.fb.array([this.buildAdresses()]),
      formeJuridique: [''],
      numeroSiren: [''],
      numeroSiret: [''],
      capitalSocial: [null],
      codeNAF: [''],
      rcs: [''],
      tvaIntra: ['']
    });

    
    if(this.telephones.length > 1)
      this.showRemoveTelBtn = true;
  
    if(this.adresses.length > 1)
      this.showRemoveAdresseBtn = true;
    
  }

  addEntreprise() {
    if(this.entrepriseForm.dirty && this.entrepriseForm.valid) {

      let e = Object.assign({}, this.entreprise, this.entrepriseForm.value);
      console.log(e);
      
    }else if(!this.entrepriseForm.dirty){
      this.onSaveComplete();
    }
  }

  onSaveComplete(){
    this.toastyService.success({
      title: 'Succès',
      msg: 'Le contact a été bien enregistré',
      theme: 'bootstrap',
      showClose: true,
      timeout: 5000
    });
    this.entrepriseForm.reset();
    //this.router.navigate(['/']);
  }

  addTelephone(){
    this.showRemoveTelBtn = true;
    this.telephones.push(this.buildTelephones());
  }

  addAdresses(){
    this.showRemoveAdresseBtn = true;
    this.adresses.push(this.buildAdresses());
  }

  removeTelephone(i){
    if(this.telephones.length > 1){
      this.telephones.removeAt(i);
    }
    if(this.telephones.length == 1){
      this.showRemoveTelBtn = false;
    }
  }

  removeAdresse(i){
    if(this.adresses.length > 1)
      this.adresses.removeAt(i);
    
    if(this.adresses.length == 1)
      this.showRemoveAdresseBtn = false;
  }

  buildTelephones() : FormGroup {
    return this.fb.group({
      IdTypeTelephone : [1, [Validators.required]],
      numero: ['', [Validators.required]]
    })
  }

  buildAdresses() : FormGroup {
    return this.fb.group({
      titre: [''],
      complement1: ['', [Validators.required]],
      complement2: [''],
      complement3: [''],
      codePostal: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      IdPays: [1, [Validators.required]],
      isAdressePrincipale: false,
      isAdresseFacturation: false,
      longitude: [''],
      latitude: ['']
    })
  }



}
