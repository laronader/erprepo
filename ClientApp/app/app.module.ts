import { AdrentrepriseComponentComponent } from './components/entreprise-comp/adrentreprise-component/adrentreprise-component.component';
import { ReferentielService } from './services/referentiel.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastyModule } from 'ng2-toasty';
import { EntrepriseService } from './services/entreprise.service';
import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { FooterSectionComponent } from './components/footer-section/footer-section.component';
import { ListeClientsComponent } from './components/liste-clients/liste-clients.component';
import { CreateEntrepriseComponent } from './components/create-entreprise/create-entreprise.component';
import { CreateContactComponent } from './components/create-contact/create-contact.component';
import { ContactService } from './services/contact.service';
import { DetailsEntrepriseComponent } from './components/details-entreprise/details-entreprise.component';
import { CommentaireComponentComponent } from './components/entreprise-comp/commentaire-component/commentaire-component.component';
import { ContactassocComponentComponent } from './components/entreprise-comp/contactassoc-component/contactassoc-component.component';
import { AgendaentrepriseComponentComponent } from './components/entreprise-comp/agendaentreprise-component/agendaentreprise-component.component';
import { TachesentrepriseComponentComponent } from './components/entreprise-comp/tachesentreprise-component/tachesentreprise-component.component';
import { EmailsentrepriseComponentComponent } from './components/entreprise-comp/emailsentreprise-component/emailsentreprise-component.component';
import { InfoentrepriseComponentComponent } from './components/entreprise-comp/infoentreprise-component/infoentreprise-component.component';
import { DocentrepriseComponentComponent } from './components/entreprise-comp/docentreprise-component/docentreprise-component.component';
import { ReglentrepriseComponentComponent } from './components/entreprise-comp/reglentreprise-component/reglentreprise-component.component';
import { CptbancentrepriseComponentComponent } from './components/entreprise-comp/cptbancentreprise-component/cptbancentreprise-component.component';


@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        FetchDataComponent,
        HomeComponent,
        TopMenuComponent,
        SideBarComponent,
        FooterSectionComponent,
        ListeClientsComponent,
        CreateEntrepriseComponent,
        CreateContactComponent,
        DetailsEntrepriseComponent,
        CommentaireComponentComponent,
        ContactassocComponentComponent,
        AgendaentrepriseComponentComponent,
        TachesentrepriseComponentComponent,
        EmailsentrepriseComponentComponent,
        InfoentrepriseComponentComponent,
        DocentrepriseComponentComponent,
        ReglentrepriseComponentComponent,
        CptbancentrepriseComponentComponent,
        AdrentrepriseComponentComponent
    ],
    imports: [
        ToastyModule.forRoot(),
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'entreprises', component: ListeClientsComponent },
            { path: 'new-entreprise', component: CreateEntrepriseComponent },
            { path: 'details-entreprise/:id', component: DetailsEntrepriseComponent, children: [

                { path: '', component: CommentaireComponentComponent, outlet: 'entreprisecomp' },
                { path: 'commentaires', component: CommentaireComponentComponent, outlet: 'entreprisecomp' },
                { path: 'contacts-associes', component: ContactassocComponentComponent, outlet: 'entreprisecomp' },
                { path: 'taches', component: TachesentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'emails', component: EmailsentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'info', component: InfoentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'agenda', component: AgendaentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'docs', component: DocentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'reglements', component: ReglentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'banque', component: CptbancentrepriseComponentComponent, outlet: 'entreprisecomp'},
                { path: 'adresses', component: AdrentrepriseComponentComponent, outlet: 'entreprisecomp'}
            ] },
            { path: 'new-contact', component: CreateContactComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'home' }
        ]),
        
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        EntrepriseService,
        ReferentielService,
        ContactService
    ]
})
export class AppModule {
}
