import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ContactService {


    constructor(private http : Http){}

    create(contact){

        
        return this.http.post('/contacts/add', contact)
            .map(res => res.json());
    }

}