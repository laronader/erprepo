import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class EntrepriseService {


    constructor(private http : Http){}

    getEntreprises() {
        return this.http.get("/entreprises/all")
            .map(res => res.json());
    }


}