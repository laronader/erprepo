import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ReferentielService {


    constructor(private http : Http){}

    getCivilites() {
        return this.http.get("/referentiels/civilites/all")
            .map(res => res.json());
    }

    getPhoneTypes() {
        return this.http.get("/referentiels/types-telephone/all")
            .map(res => res.json());
    }

    getTypeClients() {
        return this.http.get("/referentiels/type-client/all")
            .map(res => res.json());
    }
    
    getCategorieTarifaire(){
        return this.http.get("/referentiels/categorie-tarifaire/all")
            .map(res => res.json());
    }

}