using System.Threading.Tasks;
using erprepo.Persistence.Services;
using Microsoft.AspNetCore.Mvc;

namespace erprepo.Controllers
{
    [Route("/api/referentiels")]
    public class ReferentielsController : Controller
    {
        private readonly IReferentielService _service;

        public ReferentielsController(IReferentielService service)
        {
            this._service = service;
        }

        [HttpGet]
        [Route("/referentiels/types-telephone/all")]
        public async Task<IActionResult> GetAllTypeTelephone(){

            var listeTypesTelephone = await _service.GetAllTypeTelephone();

            if(listeTypesTelephone == null)
                return NotFound();

            return Ok(listeTypesTelephone);
        }

        [HttpGet]
        [Route("/referentiels/types-telephone/{id}")]
        public async Task<IActionResult> GetTypeTelephoneById(int id){

            var typeTelephone = await _service.GetTypeTelephoneById(id);

            if(typeTelephone == null)
                return NotFound();
                
            return Ok(typeTelephone);
        }

        [HttpGet]
        [Route("/referentiels/civilites/all")]
        public async Task<IActionResult> GetAllCivilite(){

             var listeCivilite = await _service.GetAllCivilite();

            if(listeCivilite == null)
                return NotFound();

            return Ok(listeCivilite);
        }

        [HttpGet]
        [Route("/referentiels/civilites/{id}")]
        public async Task<IActionResult> GetCiviliteById(int id){

            var civilite = await _service.GetCiviliteById(id);

            if(civilite == null)
                return NotFound();
                
            return Ok(civilite);
        }

        [HttpGet]
        [Route("/referentiels/categorie-tarifaire/all")]
        public async Task<IActionResult> GetAllCategorieTarifaire(){

             var listeCategorieTarifaire = await _service.GetAllCategorieTarifaire();

            if(listeCategorieTarifaire == null)
                return NotFound();

            return Ok(listeCategorieTarifaire);
        }

        [HttpGet]
        [Route("/referentiels/categorie-tarifaire/{id}")]
        public async Task<IActionResult> GetCategorieTarifaireById(int id){

            var categorieTarifaire = await _service.GetCategorieTarifaireById(id);

            if(categorieTarifaire == null)
                return NotFound();
                
            return Ok(categorieTarifaire);
        }

        [HttpGet]
        [Route("/referentiels/type-client/all")]
        public async Task<IActionResult> GetAllTypeClients()
        {
            var listeTypeClient = await _service.GetAllTypeClients();
            if(listeTypeClient == null)
                return NotFound();
            return Ok(listeTypeClient);
        }

        [HttpGet]
        [Route("/referentiels/type-client/{id}")]
        public async Task<IActionResult> GetTypeClientById(int id)
        {
            var typeClient = await _service.GetTypeClientById(id);
            if(typeClient == null)
                return NotFound();
            return Ok(typeClient);
        }
    }
}