using System.Threading.Tasks;
using erprepo.Persistence.Services;
using erprepo.Persistence.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace erprepo.Controllers
{
    [Route("/api/entreprises")]
    public class EntreprisesController : Controller
    {
        private readonly IEntrepriseService _service;

        public EntreprisesController(IEntrepriseService service)
        {
            _service = service;
        }

        /// <summary>
        /// La liste complete des entreprises
        /// </summary>
        /// <returns>EntrepriseViewModel</returns>
        [HttpGet]
        [Route("/entreprises/all")]
        public async Task<IActionResult> GetAll(){
            var liste = await _service.GetAllAsync();
            if(liste == null)
                return NotFound();
            return Ok(liste);
        }

        [HttpPost]
        [Route("/entreprise/add")]
        public IActionResult Create([FromBody] EntrepriseViewModel vm)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            
            _service.Create(vm);

            return Ok(vm);
        }

        [HttpPut]
        [Route("/entreprise/edit/{id}")]
        public IActionResult Update(int id, [FromBody] EntrepriseViewModel vm)
        {
            return Ok(vm);
        }

        [HttpDelete]
        [Route("/entreprise/delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            return Ok(id);
        }

        [HttpGet]
        [Route("/entreprise/by-id/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            EntrepriseViewModel result = await _service.GetById(id);

            if(result == null)
                return NotFound();
            
            return Ok(result);
        }

        [HttpGet]
        [Route("/entreprise/by-user/{id}")]
        public IActionResult GetByUserId(int id)
        {
            var liste = _service.GetByUserId(id);

            if(liste == null)
                return NotFound();

            return Ok(liste);
        }
    }
}