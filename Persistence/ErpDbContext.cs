using erprepo.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace erprepo.Persistence
{
    public class ErpDbContext : DbContext
    {
         public ErpDbContext(DbContextOptions<ErpDbContext> options)
            :base(options)
        {
            
        }

        public DbSet<TypeTelephone> TypeTelephones { get; set; }
        public DbSet<Civilite> Civilites { get; set; }
        public DbSet<CategorieTarifaire> CategorieTarifaires { get; set; }
        public DbSet<TypeClient> TypeClients { get; set; }
        public DbSet<Entreprise> Entreprises { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Adresse> Adresses { get; set; }
        public DbSet<Telephone> Telephones { get; set; }


        
    }
}