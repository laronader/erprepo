using System.Collections.Generic;
using System.Threading.Tasks;
using erprepo.Persistence.Models;
using erprepo.Persistence.Repositories.Referentiels;

namespace erprepo.Persistence.Services
{
    public class ReferentielService : IReferentielService
    {
        private readonly IReferentielRepository _repo;
        public ReferentielService(IReferentielRepository repo)
        {
            this._repo = repo;

        }
        public async Task<IEnumerable<TypeTelephone>> GetAllTypeTelephone()
        {
            return await _repo.GetAllTypeTelephone();
        }

        public async Task<TypeTelephone> GetTypeTelephoneById(int id)
        {
            return await _repo.GetTypeTelephoneById(id);
        }

        public async Task<IEnumerable<Civilite>> GetAllCivilite()
        {
            return await _repo.GetAllCivilite();
        }
        public async Task<Civilite> GetCiviliteById(int id)
        {
            return await _repo.GetCiviliteById(id);
        }

        public async Task<IEnumerable<CategorieTarifaire>> GetAllCategorieTarifaire()
        {
            return await _repo.GetAllCategorieTarifaire();
        }
        public async Task<CategorieTarifaire> GetCategorieTarifaireById(int id)
        {
            return await _repo.GetCategorieTarifaireById(id);
        }

        public async Task<IEnumerable<TypeClient>> GetAllTypeClients(){
            return await _repo.GetAllTypeClients();
        }

        public async Task<TypeClient> GetTypeClientById(int id)
        {
            return await _repo.GetTypeClientById(id);
        }
    }
}