using System.Collections.Generic;
using System.Threading.Tasks;
using erprepo.Persistence.Models;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Services
{
    public interface IEntrepriseService
    {
         Task<IEnumerable<EntrepriseViewModel>> GetAllAsync();
         Task<EntrepriseViewModel> GetById(int id);
         IEnumerable<EntrepriseViewModel> GetByUserId(int id);
         Task Delete(int id);
         Task Create(EntrepriseViewModel vm);
    }
}