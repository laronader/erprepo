using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using erprepo.Persistence.Models;
using erprepo.Persistence.Repositories.Generic;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Services
{
    public class EntrepriseService : IEntrepriseService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public EntrepriseService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        public async Task<IEnumerable<EntrepriseViewModel>> GetAllAsync()
        {
            var entrepriseList = await _uow.Entreprises.GetAllAsync();
            return _mapper.Map<IEnumerable<Entreprise>, IEnumerable<EntrepriseViewModel>>(entrepriseList);
        }

        public async Task<EntrepriseViewModel> GetById(int id)
        {
            var entreprise = await _uow.Entreprises.GetAsync(id);
            return _mapper.Map<Entreprise, EntrepriseViewModel>(entreprise);
        }

        public IEnumerable<EntrepriseViewModel> GetByUserId(int id)
        {
            var entreprise =  _uow.Entreprises.Find(e => e.UserCreate == id);
            return _mapper.Map<IEnumerable<Entreprise>, IEnumerable<EntrepriseViewModel>>(entreprise);
        }

        public async Task Delete(int id)
        {
            var entreprise = await _uow.Entreprises.GetAsync(id);
            //var result = _mapper.Map<Entreprise, EntrepriseViewModel>(entreprise);
            _uow.Entreprises.Remove(entreprise);
            await _uow.CompleteAsync();

        }

        public async Task Create(EntrepriseViewModel vm)
        {
            try
            {
                var entreprise = _mapper.Map<EntrepriseViewModel, Entreprise>(vm);
                entreprise.DateCreate = DateTime.Now;
                entreprise.UserCreate = 1; // to be changed
                entreprise.UniqueId = Guid.NewGuid();

                _uow.Entreprises.Add(entreprise);
                await _uow.CompleteAsync();
            }
            catch(Exception ex)
            {
                throw new Exception();
            }
            
        }
    }
}