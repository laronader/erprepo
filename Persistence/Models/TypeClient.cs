using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("TypeClient")]
    public class TypeClient
    {
        [Key]
        public int idTypeClient { get; set; }
        [Required]
        [StringLength(50)]
        public string libelle { get; set; }
    }
}