using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("Civilite")]
    public class Civilite
    {
        [Key]
        public int IdCivilite { get; set; }
        [Required]
        [StringLength(4)]
        public string LibelleShort { get; set; }
        [Required]
        [StringLength(20)]
        public string LibelleLong { get; set; }
    }
}