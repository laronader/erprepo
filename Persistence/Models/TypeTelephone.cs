using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("TypeTelephone")]
    public class TypeTelephone
    {
        [Key]
        public int IdTypeTelephone { get; set; }
        [Required]
        [StringLength(10)]
        public string Libelle { get; set; }
    }
}