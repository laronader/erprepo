using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("Telephone")]
    public class Telephone
    {
        [Key]
        public int IdTelephone { get; set; }
        public Guid UniqueId { get; set; }
        public string Numero { get; set; }
        public virtual TypeTelephone TypeTelephone { get; set; }
        public int TypeTelephoneId { get; set; }
    }
}