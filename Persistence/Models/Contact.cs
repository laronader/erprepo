using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("Contact")]
    public class Contact
    {
        public Contact()
        {
            this.Adresses = new Collection<Adresse>();
            this.Telephones = new Collection<Telephone>();
        }

        [Key]
        public int IdContact { get; set; }
        public Guid UniqueId { get; set; }
        public virtual Civilite Civilite { get; set; }
        public int CiviliteId { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Email { get; set; }
        public string Fonction { get; set; }
        public virtual ICollection<Adresse> Adresses { get; set; }
        public virtual ICollection<Telephone> Telephones { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime? DateModif { get; set; }
        public int UserCreate { get; set; }
        public int? UserModif { get; set; }
        public bool IsActif { get; set; }

    }
}