using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("Entreprise")]
    public class Entreprise 
    {
        public Entreprise()
        {
            this.Adresses = new Collection<Adresse>();
            this.Telephones = new Collection<Telephone>();
            this.Contacts = new Collection<Contact>();
        }


        [Key]
        public int IdEntreprise { get; set; }
        public Guid UniqueId { get; set; }
        public virtual ICollection<Adresse> Adresses { get; set; }
        public virtual ICollection<Telephone> Telephones { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual CategorieTarifaire CategorieTarifaire { get; set; }
        public int CategorieTarifaireId { get; set; }
        public string RaisonSociale { get; set; }
        public string Reference { get; set; }
        public string SiteWeb { get; set; }
        public string NumeroSiren { get; set; }
        public string NumeroSiret { get; set; }
        public string CodeNAF { get; set; }
        public decimal CapitalSocial { get; set; }
        public string RCS { get; set; }
        public string NumTVAIntra { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime? DateModif { get; set; }
        public int UserCreate { get; set; }
        public int? UserModif { get; set; }
        public bool IsActif { get; set; }
    }
}