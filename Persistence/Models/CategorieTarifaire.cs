using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("CategorieTarifaire")]
    public class CategorieTarifaire
    {
        [Key]
        public int IdCategorieTarifaire { get; set; }
        [Required]
        [StringLength(20)]
        public string Libelle { get; set; }
    }
}