using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace erprepo.Persistence.Models
{
    [Table("Adresse")]
    public class Adresse
    {
        [Key]
        public int IdAdresse { get; set; }
        public Guid UniqueId { get; set; }
        public bool IsPrincipale { get; set; }
        public string Libelle { get; set; }
        public string Complement_1 { get; set; }
        public string Complement_2 { get; set; }
        public string Complement_3 { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}