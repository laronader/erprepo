using AutoMapper;
using erprepo.Persistence.Models;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Mappings
{
    public class MappingEntreprise : Profile
    {
        public MappingEntreprise()
        {
            CreateMap<Entreprise, EntrepriseViewModel>();
                
            CreateMap<EntrepriseViewModel, Entreprise>()
                .ForMember(e => e.IdEntreprise, opt => opt.Ignore())
                .ForMember(e => e.UniqueId, opt => opt.Ignore()); 
        }
    }
}