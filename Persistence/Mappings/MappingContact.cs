using AutoMapper;
using erprepo.Persistence.Models;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Mappings
{
    public class MappingContact : Profile
    {
        public MappingContact()
        {
            CreateMap<Contact, ContactViewModel>();

            CreateMap<ContactViewModel, Contact>();
        }
    }
}