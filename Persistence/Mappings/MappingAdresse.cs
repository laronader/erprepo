using AutoMapper;
using erprepo.Persistence.Models;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Mappings
{
    public class MappingAdresse : Profile
    {
        public MappingAdresse()
        {
            CreateMap<Adresse, AdresseViewModel>();

            CreateMap<AdresseViewModel, Adresse>();
        }
    }
}