using AutoMapper;
using erprepo.Persistence.Models;
using erprepo.Persistence.ViewModels;

namespace erprepo.Persistence.Mappings
{
    public class MappingTelephone : Profile
    {
        public MappingTelephone()
        {
            CreateMap<Telephone, TelephoneViewModel>();

            CreateMap<TelephoneViewModel, Telephone>();
        }
    }
}