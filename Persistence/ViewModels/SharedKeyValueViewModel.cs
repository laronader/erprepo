namespace erprepo.Persistence.ViewModels
{
    public class SharedKeyValueViewModel
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}