using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace erprepo.Persistence.ViewModels
{
    public class ContactViewModel
    {
        public ContactViewModel()
        {
            this.adresses = new Collection<AdresseViewModel>();
            this.telephones = new Collection<TelephoneViewModel>();
        }

        public int IdContact { get; set; }
        public Guid UniqueId { get; set; }
        public string prenom { get; set; }
        public string nom { get; set; }
        public string email { get; set; }
        public string fonction { get; set; }
        public virtual ICollection<AdresseViewModel> adresses { get; set; }
        public virtual ICollection<TelephoneViewModel> telephones { get; set; }
        public string facebookUrl { get; set; }
        public string twitterUrl { get; set; }
        public string linkedinUrl { get; set; }
        public string gplusUrl { get; set; }
        public string siteWeb { get; set; }
        public bool IsActif { get; set; }
    }
}