using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace erprepo.Persistence.ViewModels
{
    public class EntrepriseViewModel
    {
        public EntrepriseViewModel()
        {
            this.Adresses = new Collection<AdresseViewModel>();
            this.Telephones = new Collection<TelephoneViewModel>();
            this.Contacts = new Collection<ContactViewModel>();
        }

        public int IdEntreprise { get; set; }
        public Guid UniqueId { get; set; }
        public virtual ICollection<AdresseViewModel> Adresses { get; set; }
        public virtual ICollection<TelephoneViewModel> Telephones { get; set; }
        public virtual ICollection<ContactViewModel> Contacts { get; set; }
        public string RaisonSociale { get; set; }
        public string Reference { get; set; }
        public string SiteWeb { get; set; }
        public string NumeroSiren { get; set; }
        public string NumeroSiret { get; set; }
        public string CodeNAF { get; set; }
        public decimal CapitalSocial { get; set; }
        public string RCS { get; set; }
        public string NumTVAIntra { get; set; }
        public bool IsActif { get; set; }
        public int CategorieTarifaireId { get; set; }
    }
}