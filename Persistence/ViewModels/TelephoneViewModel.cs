using System;

namespace erprepo.Persistence.ViewModels
{
    public class TelephoneViewModel
    {
        public int IdTelephone { get; set; }
        public Guid UniqueId { get; set; }
        public string Numero { get; set; }
    }
}