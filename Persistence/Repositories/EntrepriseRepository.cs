using erprepo.Persistence.Models;
using erprepo.Persistence.Repositories.Generic;

namespace erprepo.Persistence.Repositories
{
    public class EntrepriseRepository : Repository<Entreprise>, IEntrepriseRepository
    {
        public EntrepriseRepository(ErpDbContext context) 
            : base(context)
        {
        }

        public ErpDbContext ErpDbContext 
        { 
            get { return _context as ErpDbContext; }
        }
    }
}