using erprepo.Persistence.Models;
using erprepo.Persistence.Repositories.Generic;

namespace erprepo.Persistence.Repositories
{
    public interface IEntrepriseRepository : IRepository<Entreprise>
    {
         
    }
}