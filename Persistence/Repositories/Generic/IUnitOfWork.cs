using System;
using System.Threading.Tasks;

namespace erprepo.Persistence.Repositories.Generic
{
    public interface IUnitOfWork : IDisposable
    {
         IEntrepriseRepository Entreprises { get; }

         Task<int> CompleteAsync();
    }
}
