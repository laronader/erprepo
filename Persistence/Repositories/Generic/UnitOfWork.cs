using System.Threading.Tasks;

namespace erprepo.Persistence.Repositories.Generic
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ErpDbContext _context;
        public UnitOfWork(ErpDbContext context)
        {
            _context = context;
            Entreprises = new EntrepriseRepository(_context);
        }

        public IEntrepriseRepository Entreprises { get; private set; }

        public async Task<int> CompleteAsync(){

            return await _context.SaveChangesAsync();
        }

        public void Dispose(){
            _context.Dispose();
        }
    }
}