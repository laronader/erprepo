using System.Collections.Generic;
using System.Threading.Tasks;
using erprepo.Persistence;
using erprepo.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace erprepo.Persistence.Repositories.Referentiels
{
    public class ReferentielRepository : IReferentielRepository
    {
        private readonly ErpDbContext _context;

        public ReferentielRepository(ErpDbContext context)
        {
            this._context = context;
        }
        public async Task<IEnumerable<TypeTelephone>> GetAllTypeTelephone()
        {
            return await _context.TypeTelephones.ToListAsync();
        }

        public async Task<TypeTelephone> GetTypeTelephoneById(int id)
        {
            return await _context.TypeTelephones.SingleOrDefaultAsync(p => p.IdTypeTelephone == id);
        }

        public async Task<IEnumerable<Civilite>> GetAllCivilite()
        {
            return await _context.Civilites.ToListAsync();
        }
        public async Task<Civilite> GetCiviliteById(int id)
        {
            return await _context.Civilites.SingleOrDefaultAsync(p => p.IdCivilite == id);
        }

        public async Task<IEnumerable<CategorieTarifaire>> GetAllCategorieTarifaire()
        {
            return await _context.CategorieTarifaires.ToListAsync();
        }
        public async Task<CategorieTarifaire> GetCategorieTarifaireById(int id)
        {
            return await _context.CategorieTarifaires.SingleOrDefaultAsync(p => p.IdCategorieTarifaire == id);
        }

        public async Task<IEnumerable<TypeClient>> GetAllTypeClients()
        {
            return await _context.TypeClients.ToListAsync();
        }

        public async Task<TypeClient> GetTypeClientById(int id)
        {
            return await _context.TypeClients.SingleOrDefaultAsync(t => t.idTypeClient == id);
        }
    }
}