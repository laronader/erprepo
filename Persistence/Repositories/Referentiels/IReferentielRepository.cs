using System.Collections.Generic;
using System.Threading.Tasks;
using erprepo.Persistence.Models;

namespace erprepo.Persistence.Repositories.Referentiels
{
    public interface IReferentielRepository
    {
         Task<IEnumerable<TypeTelephone>> GetAllTypeTelephone();
         Task<TypeTelephone> GetTypeTelephoneById(int id);

         Task<IEnumerable<Civilite>> GetAllCivilite();
         Task<Civilite> GetCiviliteById(int id);

         Task<IEnumerable<CategorieTarifaire>> GetAllCategorieTarifaire();
         Task<CategorieTarifaire> GetCategorieTarifaireById(int id);

         Task<IEnumerable<TypeClient>> GetAllTypeClients();
         Task<TypeClient> GetTypeClientById(int id);
    }
}