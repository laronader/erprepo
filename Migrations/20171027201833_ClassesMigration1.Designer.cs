﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using erprepo.Persistence;

namespace Erp.Migrations
{
    [DbContext(typeof(ErpDbContext))]
    [Migration("20171027201833_ClassesMigration1")]
    partial class ClassesMigration1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("erprepo.Persistence.Models.Adresse", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("CodePostal");

                    b.Property<string>("Complement_1");

                    b.Property<string>("Complement_2");

                    b.Property<string>("Complement_3");

                    b.Property<bool>("IsPrincipale");

                    b.Property<string>("Latitude");

                    b.Property<string>("Libelle");

                    b.Property<string>("Longitude");

                    b.Property<Guid>("UniqueId");

                    b.Property<string>("Ville");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("Adresse");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.CategorieTarifaire", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("CategorieTarifaire");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Civilite", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LibelleLong")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("LibelleShort")
                        .IsRequired()
                        .HasMaxLength(4);

                    b.HasKey("Id");

                    b.ToTable("Civilite");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreate");

                    b.Property<DateTime?>("DateModif");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<bool>("IsActif");

                    b.Property<bool>("IsParticulier");

                    b.Property<Guid>("UniqueId");

                    b.Property<int>("UserCreate");

                    b.Property<int?>("UserModif");

                    b.HasKey("Id");

                    b.ToTable("Client");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Client");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Telephone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClientId");

                    b.Property<string>("Numero");

                    b.Property<Guid>("UniqueId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("Telephone");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.TypeTelephone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("Id");

                    b.ToTable("TypeTelephone");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Contact", b =>
                {
                    b.HasBaseType("erprepo.Persistence.Models.Client");

                    b.Property<string>("Email");

                    b.Property<string>("Nom");

                    b.Property<string>("Prenom");

                    b.ToTable("Contact");

                    b.HasDiscriminator().HasValue("Contact");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Entreprise", b =>
                {
                    b.HasBaseType("erprepo.Persistence.Models.Client");

                    b.Property<decimal>("CapitalSocial");

                    b.Property<string>("CodeNAF");

                    b.Property<string>("NumTVAIntra");

                    b.Property<string>("NumeroSiren");

                    b.Property<string>("NumeroSiret");

                    b.Property<string>("RCS");

                    b.Property<string>("RaisonSociale");

                    b.Property<string>("Reference");

                    b.Property<string>("SiteWeb");

                    b.ToTable("Entreprise");

                    b.HasDiscriminator().HasValue("Entreprise");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Adresse", b =>
                {
                    b.HasOne("erprepo.Persistence.Models.Client", "Client")
                        .WithMany("Adresses")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Telephone", b =>
                {
                    b.HasOne("erprepo.Persistence.Models.Client", "Client")
                        .WithMany("Telephones")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
