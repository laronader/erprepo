﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Erp.Migrations
{
    public partial class ClassesMigrationTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Adresse_Client_ClientId",
                table: "Adresse");

            migrationBuilder.DropForeignKey(
                name: "FK_Telephone_Client_ClientId",
                table: "Telephone");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropIndex(
                name: "IX_Telephone_ClientId",
                table: "Telephone");

            migrationBuilder.DropIndex(
                name: "IX_Adresse_ClientId",
                table: "Adresse");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Telephone");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Adresse");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "TypeTelephone",
                newName: "IdTypeTelephone");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Telephone",
                newName: "IdTelephone");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Civilite",
                newName: "IdCivilite");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "CategorieTarifaire",
                newName: "IdCategorieTarifaire");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Adresse",
                newName: "IdAdresse");

            migrationBuilder.AddColumn<int>(
                name: "ContactIdContact",
                table: "Telephone",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntrepriseIdEntreprise",
                table: "Telephone",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactIdContact",
                table: "Adresse",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntrepriseIdEntreprise",
                table: "Adresse",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Entreprise",
                columns: table => new
                {
                    IdEntreprise = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CapitalSocial = table.Column<decimal>(nullable: false),
                    CodeNAF = table.Column<string>(nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModif = table.Column<DateTime>(nullable: true),
                    IsActif = table.Column<bool>(nullable: false),
                    NumTVAIntra = table.Column<string>(nullable: true),
                    NumeroSiren = table.Column<string>(nullable: true),
                    NumeroSiret = table.Column<string>(nullable: true),
                    RCS = table.Column<string>(nullable: true),
                    RaisonSociale = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    SiteWeb = table.Column<string>(nullable: true),
                    UniqueId = table.Column<Guid>(nullable: false),
                    UserCreate = table.Column<int>(nullable: false),
                    UserModif = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Entreprise", x => x.IdEntreprise);
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    IdContact = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModif = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EntrepriseIdEntreprise = table.Column<int>(nullable: true),
                    Fonction = table.Column<string>(nullable: true),
                    IsActif = table.Column<bool>(nullable: false),
                    Nom = table.Column<string>(nullable: true),
                    Prenom = table.Column<string>(nullable: true),
                    UniqueId = table.Column<Guid>(nullable: false),
                    UserCreate = table.Column<int>(nullable: false),
                    UserModif = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.IdContact);
                    table.ForeignKey(
                        name: "FK_Contact_Entreprise_EntrepriseIdEntreprise",
                        column: x => x.EntrepriseIdEntreprise,
                        principalTable: "Entreprise",
                        principalColumn: "IdEntreprise",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_ContactIdContact",
                table: "Telephone",
                column: "ContactIdContact");

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_EntrepriseIdEntreprise",
                table: "Telephone",
                column: "EntrepriseIdEntreprise");

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_ContactIdContact",
                table: "Adresse",
                column: "ContactIdContact");

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_EntrepriseIdEntreprise",
                table: "Adresse",
                column: "EntrepriseIdEntreprise");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_EntrepriseIdEntreprise",
                table: "Contact",
                column: "EntrepriseIdEntreprise");

            migrationBuilder.AddForeignKey(
                name: "FK_Adresse_Contact_ContactIdContact",
                table: "Adresse",
                column: "ContactIdContact",
                principalTable: "Contact",
                principalColumn: "IdContact",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Adresse_Entreprise_EntrepriseIdEntreprise",
                table: "Adresse",
                column: "EntrepriseIdEntreprise",
                principalTable: "Entreprise",
                principalColumn: "IdEntreprise",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Telephone_Contact_ContactIdContact",
                table: "Telephone",
                column: "ContactIdContact",
                principalTable: "Contact",
                principalColumn: "IdContact",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Telephone_Entreprise_EntrepriseIdEntreprise",
                table: "Telephone",
                column: "EntrepriseIdEntreprise",
                principalTable: "Entreprise",
                principalColumn: "IdEntreprise",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Adresse_Contact_ContactIdContact",
                table: "Adresse");

            migrationBuilder.DropForeignKey(
                name: "FK_Adresse_Entreprise_EntrepriseIdEntreprise",
                table: "Adresse");

            migrationBuilder.DropForeignKey(
                name: "FK_Telephone_Contact_ContactIdContact",
                table: "Telephone");

            migrationBuilder.DropForeignKey(
                name: "FK_Telephone_Entreprise_EntrepriseIdEntreprise",
                table: "Telephone");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropTable(
                name: "Entreprise");

            migrationBuilder.DropIndex(
                name: "IX_Telephone_ContactIdContact",
                table: "Telephone");

            migrationBuilder.DropIndex(
                name: "IX_Telephone_EntrepriseIdEntreprise",
                table: "Telephone");

            migrationBuilder.DropIndex(
                name: "IX_Adresse_ContactIdContact",
                table: "Adresse");

            migrationBuilder.DropIndex(
                name: "IX_Adresse_EntrepriseIdEntreprise",
                table: "Adresse");

            migrationBuilder.DropColumn(
                name: "ContactIdContact",
                table: "Telephone");

            migrationBuilder.DropColumn(
                name: "EntrepriseIdEntreprise",
                table: "Telephone");

            migrationBuilder.DropColumn(
                name: "ContactIdContact",
                table: "Adresse");

            migrationBuilder.DropColumn(
                name: "EntrepriseIdEntreprise",
                table: "Adresse");

            migrationBuilder.RenameColumn(
                name: "IdTypeTelephone",
                table: "TypeTelephone",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "IdTelephone",
                table: "Telephone",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "IdCivilite",
                table: "Civilite",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "IdCategorieTarifaire",
                table: "CategorieTarifaire",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "IdAdresse",
                table: "Adresse",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "Telephone",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "Adresse",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModif = table.Column<DateTime>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    IsActif = table.Column<bool>(nullable: false),
                    IsParticulier = table.Column<bool>(nullable: false),
                    UniqueId = table.Column<Guid>(nullable: false),
                    UserCreate = table.Column<int>(nullable: false),
                    UserModif = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EntrepriseId = table.Column<int>(nullable: true),
                    Nom = table.Column<string>(nullable: true),
                    Prenom = table.Column<string>(nullable: true),
                    CapitalSocial = table.Column<decimal>(nullable: true),
                    CodeNAF = table.Column<string>(nullable: true),
                    NumTVAIntra = table.Column<string>(nullable: true),
                    NumeroSiren = table.Column<string>(nullable: true),
                    NumeroSiret = table.Column<string>(nullable: true),
                    RCS = table.Column<string>(nullable: true),
                    RaisonSociale = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    SiteWeb = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Client_Client_EntrepriseId",
                        column: x => x.EntrepriseId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_ClientId",
                table: "Telephone",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_ClientId",
                table: "Adresse",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Client_EntrepriseId",
                table: "Client",
                column: "EntrepriseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Adresse_Client_ClientId",
                table: "Adresse",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Telephone_Client_ClientId",
                table: "Telephone",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
