﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using erprepo.Persistence;

namespace Erp.Migrations
{
    [DbContext(typeof(ErpDbContext))]
    [Migration("20171027181049_ReferentielsMigrationV1")]
    partial class ReferentielsMigrationV1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("erprepo.Persistence.Models.CategorieTarifaire", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("CategorieTarifaire");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Civilite", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LibelleLong")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("LibelleShort")
                        .IsRequired()
                        .HasMaxLength(4);

                    b.HasKey("Id");

                    b.ToTable("Civilite");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.TypeTelephone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("Id");

                    b.ToTable("TypeTelephone");
                });
        }
    }
}
