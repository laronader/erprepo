﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Migrations
{
    public partial class ClassesMigrationTest1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TypeTelephoneId",
                table: "Telephone",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CategorieTarifaireId",
                table: "Entreprise",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CiviliteId",
                table: "Contact",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_TypeTelephoneId",
                table: "Telephone",
                column: "TypeTelephoneId");

            migrationBuilder.CreateIndex(
                name: "IX_Entreprise_CategorieTarifaireId",
                table: "Entreprise",
                column: "CategorieTarifaireId");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_CiviliteId",
                table: "Contact",
                column: "CiviliteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_Civilite_CiviliteId",
                table: "Contact",
                column: "CiviliteId",
                principalTable: "Civilite",
                principalColumn: "IdCivilite",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Entreprise_CategorieTarifaire_CategorieTarifaireId",
                table: "Entreprise",
                column: "CategorieTarifaireId",
                principalTable: "CategorieTarifaire",
                principalColumn: "IdCategorieTarifaire",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Telephone_TypeTelephone_TypeTelephoneId",
                table: "Telephone",
                column: "TypeTelephoneId",
                principalTable: "TypeTelephone",
                principalColumn: "IdTypeTelephone",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_Civilite_CiviliteId",
                table: "Contact");

            migrationBuilder.DropForeignKey(
                name: "FK_Entreprise_CategorieTarifaire_CategorieTarifaireId",
                table: "Entreprise");

            migrationBuilder.DropForeignKey(
                name: "FK_Telephone_TypeTelephone_TypeTelephoneId",
                table: "Telephone");

            migrationBuilder.DropIndex(
                name: "IX_Telephone_TypeTelephoneId",
                table: "Telephone");

            migrationBuilder.DropIndex(
                name: "IX_Entreprise_CategorieTarifaireId",
                table: "Entreprise");

            migrationBuilder.DropIndex(
                name: "IX_Contact_CiviliteId",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "TypeTelephoneId",
                table: "Telephone");

            migrationBuilder.DropColumn(
                name: "CategorieTarifaireId",
                table: "Entreprise");

            migrationBuilder.DropColumn(
                name: "CiviliteId",
                table: "Contact");
        }
    }
}
