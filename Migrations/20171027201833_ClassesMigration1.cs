﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Erp.Migrations
{
    public partial class ClassesMigration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModif = table.Column<DateTime>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    IsActif = table.Column<bool>(nullable: false),
                    IsParticulier = table.Column<bool>(nullable: false),
                    UniqueId = table.Column<Guid>(nullable: false),
                    UserCreate = table.Column<int>(nullable: false),
                    UserModif = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Nom = table.Column<string>(nullable: true),
                    Prenom = table.Column<string>(nullable: true),
                    CapitalSocial = table.Column<decimal>(nullable: true),
                    CodeNAF = table.Column<string>(nullable: true),
                    NumTVAIntra = table.Column<string>(nullable: true),
                    NumeroSiren = table.Column<string>(nullable: true),
                    NumeroSiret = table.Column<string>(nullable: true),
                    RCS = table.Column<string>(nullable: true),
                    RaisonSociale = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    SiteWeb = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Adresse",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    CodePostal = table.Column<string>(nullable: true),
                    Complement_1 = table.Column<string>(nullable: true),
                    Complement_2 = table.Column<string>(nullable: true),
                    Complement_3 = table.Column<string>(nullable: true),
                    IsPrincipale = table.Column<bool>(nullable: false),
                    Latitude = table.Column<string>(nullable: true),
                    Libelle = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    UniqueId = table.Column<Guid>(nullable: false),
                    Ville = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adresse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Adresse_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Telephone",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    Numero = table.Column<string>(nullable: true),
                    UniqueId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Telephone", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Telephone_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_ClientId",
                table: "Adresse",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_ClientId",
                table: "Telephone",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adresse");

            migrationBuilder.DropTable(
                name: "Telephone");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
