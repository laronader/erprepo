﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Migrations
{
    public partial class ClassesMigration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EntrepriseId",
                table: "Client",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Client_EntrepriseId",
                table: "Client",
                column: "EntrepriseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Client_Client_EntrepriseId",
                table: "Client",
                column: "EntrepriseId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Client_Client_EntrepriseId",
                table: "Client");

            migrationBuilder.DropIndex(
                name: "IX_Client_EntrepriseId",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "EntrepriseId",
                table: "Client");
        }
    }
}
