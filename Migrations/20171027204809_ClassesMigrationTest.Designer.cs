﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using erprepo.Persistence;

namespace Erp.Migrations
{
    [DbContext(typeof(ErpDbContext))]
    [Migration("20171027204809_ClassesMigrationTest")]
    partial class ClassesMigrationTest
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("erprepo.Persistence.Models.Adresse", b =>
                {
                    b.Property<int>("IdAdresse")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodePostal");

                    b.Property<string>("Complement_1");

                    b.Property<string>("Complement_2");

                    b.Property<string>("Complement_3");

                    b.Property<int?>("ContactIdContact");

                    b.Property<int?>("EntrepriseIdEntreprise");

                    b.Property<bool>("IsPrincipale");

                    b.Property<string>("Latitude");

                    b.Property<string>("Libelle");

                    b.Property<string>("Longitude");

                    b.Property<Guid>("UniqueId");

                    b.Property<string>("Ville");

                    b.HasKey("IdAdresse");

                    b.HasIndex("ContactIdContact");

                    b.HasIndex("EntrepriseIdEntreprise");

                    b.ToTable("Adresse");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.CategorieTarifaire", b =>
                {
                    b.Property<int>("IdCategorieTarifaire")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.HasKey("IdCategorieTarifaire");

                    b.ToTable("CategorieTarifaire");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Civilite", b =>
                {
                    b.Property<int>("IdCivilite")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LibelleLong")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("LibelleShort")
                        .IsRequired()
                        .HasMaxLength(4);

                    b.HasKey("IdCivilite");

                    b.ToTable("Civilite");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Contact", b =>
                {
                    b.Property<int>("IdContact")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreate");

                    b.Property<DateTime?>("DateModif");

                    b.Property<string>("Email");

                    b.Property<int?>("EntrepriseIdEntreprise");

                    b.Property<string>("Fonction");

                    b.Property<bool>("IsActif");

                    b.Property<string>("Nom");

                    b.Property<string>("Prenom");

                    b.Property<Guid>("UniqueId");

                    b.Property<int>("UserCreate");

                    b.Property<int?>("UserModif");

                    b.HasKey("IdContact");

                    b.HasIndex("EntrepriseIdEntreprise");

                    b.ToTable("Contact");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Entreprise", b =>
                {
                    b.Property<int>("IdEntreprise")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("CapitalSocial");

                    b.Property<string>("CodeNAF");

                    b.Property<DateTime>("DateCreate");

                    b.Property<DateTime?>("DateModif");

                    b.Property<bool>("IsActif");

                    b.Property<string>("NumTVAIntra");

                    b.Property<string>("NumeroSiren");

                    b.Property<string>("NumeroSiret");

                    b.Property<string>("RCS");

                    b.Property<string>("RaisonSociale");

                    b.Property<string>("Reference");

                    b.Property<string>("SiteWeb");

                    b.Property<Guid>("UniqueId");

                    b.Property<int>("UserCreate");

                    b.Property<int?>("UserModif");

                    b.HasKey("IdEntreprise");

                    b.ToTable("Entreprise");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Telephone", b =>
                {
                    b.Property<int>("IdTelephone")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactIdContact");

                    b.Property<int?>("EntrepriseIdEntreprise");

                    b.Property<string>("Numero");

                    b.Property<Guid>("UniqueId");

                    b.HasKey("IdTelephone");

                    b.HasIndex("ContactIdContact");

                    b.HasIndex("EntrepriseIdEntreprise");

                    b.ToTable("Telephone");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.TypeTelephone", b =>
                {
                    b.Property<int>("IdTypeTelephone")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("IdTypeTelephone");

                    b.ToTable("TypeTelephone");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Adresse", b =>
                {
                    b.HasOne("erprepo.Persistence.Models.Contact")
                        .WithMany("Adresses")
                        .HasForeignKey("ContactIdContact");

                    b.HasOne("erprepo.Persistence.Models.Entreprise")
                        .WithMany("Adresses")
                        .HasForeignKey("EntrepriseIdEntreprise");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Contact", b =>
                {
                    b.HasOne("erprepo.Persistence.Models.Entreprise")
                        .WithMany("Contacts")
                        .HasForeignKey("EntrepriseIdEntreprise");
                });

            modelBuilder.Entity("erprepo.Persistence.Models.Telephone", b =>
                {
                    b.HasOne("erprepo.Persistence.Models.Contact")
                        .WithMany("Telephones")
                        .HasForeignKey("ContactIdContact");

                    b.HasOne("erprepo.Persistence.Models.Entreprise")
                        .WithMany("Telephones")
                        .HasForeignKey("EntrepriseIdEntreprise");
                });
        }
    }
}
